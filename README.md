# Database - Team Neo
https://gitlab.com/haroutdabagh/database-team-neo

# About
In this project we are going to use JDBC to connect to our database, once the user gets the connection he will have a menu in which he will display four options. 

1- is to query - we will display some options to the user of what the query, the user will choose the one he wants and after the program will request the input and call the procedure in our database and it will return the results to the console.

2 - is to insert - we will display some options to the user of what to insert into the database, the user will choose the one he wants and then the program will ask for the new entry that the user likes to add and call the corresponding procedure in the database and add them.

3 - is to delete - we will display some options to the user of what to delete from the database, the user will choose the one he wants and then the program will ask for the entry that the user likes to remove and call the corresponding procedure in the database and delete them.

4 - exit - terminate program

# Setup
Add ojdbc-10 to your classpath (included in the lib folder), compile all java files in the src.

## SQL BUILD INSTRUCTIONS
Please read these instructions first before building. Thank you.

## Table building
Table building is done as a necessary and first step for the database's structure. Always run this first.

1. If you are re-building the database, make sure to run table-drop.sql first.
2. Next, you want to run table-build.sql to build the tables. This does not populate them
3. Run default-procedures.sql (for the next step) 
4. You want then run insert-triggers.sql for the triggers. (This uses a procedure to validate data, so that's why you need to do the above!)
5. Feel free to populate the database or to insert packages after this.

## Packages
The packages here are used to allow for inserting, fetching and deleting data.

1. Run package-check.sql before running any other package script 
2. Now you can run package-update.sql, package-read.sql and package-delete.sql to add the remaining packages

## Populating
1. Run the table-populate.sql script. Make some popcorn or tea, though. This might take awhile.


import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Types;

public class QueryData {
    private DatabaseConnection dbConnection;

    public QueryData(DatabaseConnection dConnection) {
        this.dbConnection = dConnection;
    }

    /**
     * Gets the compilation of performances
     * @param id performance id
     * @throws SQLException
     */
    public void getCompilationPerformances(int id) throws SQLException {
        Connection connection = dbConnection.getConnection();
        CallableStatement cs = connection.prepareCall("call fetch_music_data.get_compilation_performances(?, ?)");
        cs.setInt(1, id);
        cs.registerOutParameter(2, Types.REF_CURSOR);
        cs.execute();
        ResultSet rs = cs.getObject(2, ResultSet.class);
        printResultSet(rs);
        cs.close();
    }
    
    /**
    * Get the contributors and roles of a recording
    * @param id recording id
    * @return
    * @throws SQLException
    */
    public void getContributorsAndRoles(int id) throws SQLException {
        Connection connection = dbConnection.getConnection();
        CallableStatement cs = connection.prepareCall("call fetch_music_data.get_performance_contributons(?, ?)");
        cs.setInt(1, id);
        cs.registerOutParameter(2, Types.REF_CURSOR);
        cs.execute();
        ResultSet rs = cs.getObject(2, ResultSet.class);
        printResultSet(rs);
        cs.close();
    }

    /**
     * Get the contributions of a contributor
     * @param id recording id
     * @throws SQLException
     */
    public void getContributorContributors(int id) throws SQLException {
        Connection connection = dbConnection.getConnection();
        CallableStatement cs = connection.prepareCall("call fetch_music_data.get_contributor_contributions(?, ?)");
        cs.setInt(1, id);
        cs.registerOutParameter(2, Types.REF_CURSOR);
        cs.execute();
        ResultSet rs = cs.getObject(2, ResultSet.class);
        printResultSet(rs);
        cs.close();
    }

    /**
     * Get the performances of a recording
     * @param id recording id
     * @throws SQLException
     */
    public void getContributorRoles(int id) throws SQLException {
        Connection connection = dbConnection.getConnection();
        CallableStatement cs = connection.prepareCall("call fetch_music_data.get_all_contributor_roles(?, ?)");
        cs.setInt(1, id);
        cs.registerOutParameter(2, Types.REF_CURSOR);
        cs.execute();
        ResultSet rs = cs.getObject(2, ResultSet.class);
        printResultSet(rs);
        cs.close();
    }

    /***
     * Prints all rows in a result set
     * @param rs result set to print
     * @throws SQLException
     */
    private void printResultSet(ResultSet rs) throws SQLException {
        ResultSetMetaData rsmd = rs.getMetaData();
        int columnsNumber = rsmd.getColumnCount();
        while (rs.next()) {
            for (int i = 1; i <= columnsNumber; i++) {
                if (i > 1) System.out.print(",  ");
                String columnValue = rs.getString(i);
                System.out.print(rsmd.getColumnName(i) + ": " + columnValue);
            }
            System.out.println("");
        }
    }
}
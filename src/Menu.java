import java.util.Scanner;

public class Menu {

    /***
     * Displays the menu to query or fetch and returns the user's choice.
     * @return option choosed
     */
    public static int databaseActions() {
        System.out.println("""
        1. Query
        2. Insert
        3. Delete
        4. Exit
        """);
        return getInput();
    }


    /***
     * Displays what teh user can query
     * @return option choosed
     */
    public static int displayQueryOptions() {
        System.out.println("""
        Type the number
        1. Get performances of a compilation
        2. Get performance contributions
        3. Get contributor contributions
        4. Get all contributors roles
        """);

        return getInput();
    }

    /***
     * Displays what the user can insert
     * @return option choosed
     */
    public static int displayInsertMenu() {
        System.out.println("""
        1. Insert a new recording
        2. Insert a new performance
        3. Insert a new contributor
        4. Insert a new contribution
        """);
        return getInput();
    }

    /***
     * Display what the user can delete
     * @return option
     */
    public static int displayDeleteMenu() {
        System.out.println("""
        1. Delete a recording
        2. Delete a performance
        3. Delete a contributor
        4. Delete a contribution
        """);
        return getInput();
    }

    /**
     * Get option from user
     * @return option
     */
    private static int getInput() {
        Scanner scanner = new Scanner(System.in);
        return scanner.nextInt();
    }
}

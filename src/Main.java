import java.sql.SQLException;

public class Main {
    private static final String url = "jdbc:oracle:thin:@pdbora19c.dawsoncollege.qc.ca:1521/pdbora19c.dawsoncollege.qc.ca";
    
    public static void main(String[] args) {
        // get username trough console
        System.out.println("Enter username: ");
        String username = System.console().readLine();
        
        // get password trough console
        System.out.println("Enter password:");
        String password = new String(System.console().readPassword());
        
        DatabaseConnection dConnection = null;
        try {
            dConnection = new DatabaseConnection(url, username, password);
        } catch (SQLException e) {
            System.err.println("Error: " + e.getLocalizedMessage());
            return;
        }
        
        try {
            dConnection.getConnection().setAutoCommit(false);
        } catch (SQLException e) {
            System.err.println("Error: " + e.getLocalizedMessage());
        }
        
        /***
        * Display tbe user options (query-insert-delete-exit)
        * @call option
        */
        boolean exit = false;
        while (!exit) {
            int databaseAction = Menu.databaseActions();
            
            try {
                switch (databaseAction) {
                    case 1 -> {
                        QueryData queryData = new QueryData(dConnection);
                        query(queryData);
                    }
                    case 2 -> {
                        InsertData insertData = new InsertData(dConnection);
                        insert(insertData, dConnection);
                    }
                    case 3 -> {
                        DeleteData deleteData = new DeleteData(dConnection);
                        deleteData(deleteData, dConnection);
                    }
                    case 4 -> exit = true;
                }
            } catch (SQLException e) {
                System.err.println("Error: " + e.getLocalizedMessage());
                System.exit(1);
                return;
            }
        }
    }
    /***
    * Display what the user can query from the database
    * @call option
    */
    
    private static void query(QueryData queryData) throws SQLException {
        int option = Menu.displayQueryOptions();
        int id = Utils.getID();
        
        switch (option) {
            case 1 -> queryData.getCompilationPerformances(id);
            case 2 -> queryData.getContributorsAndRoles(id);
            case 3 -> queryData.getContributorContributors(id);
            case 4 -> queryData.getContributorRoles(id);
            default -> throw new IllegalArgumentException("Unexpected value: " + option);
        }
    }
    /***
    * Display what the user can inset into the database
    * @call option
    */
    public static void insert(InsertData insertData, DatabaseConnection dConnection) throws SQLException {
        int option = Menu.displayInsertMenu();
        
        switch (option) {
            case 1 -> insertData.insertRecording();
            case 2 -> insertData.insertPerformance();
            case 3 -> insertData.insertNewContributor();
            case 4 -> insertData.insertNewContribution();
            default -> throw new IllegalArgumentException("Unexpected value: " + option);
        }
        dConnection.getConnection().commit();
        System.out.println("Insertion successful");
    }
    /***
    * Display what the user can delete from the database
    * @call option
    */
    public static void deleteData(DeleteData deleteData, DatabaseConnection dConnection) throws SQLException {
        int option = Menu.displayDeleteMenu();
        
        switch (option) {
            case 1 -> deleteData.deleteRecording();
            case 2 -> deleteData.deletePerformance();
            case 3 -> deleteData.deleteContributor();
            case 4 -> deleteData.deleteContribution();
            default -> throw new IllegalArgumentException("Unexpected value: " + option);
        }
        dConnection.getConnection().commit();
        System.out.println("Deletion successful");
    }
}

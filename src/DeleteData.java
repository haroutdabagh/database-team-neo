import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Scanner;

public class DeleteData {
    private Connection connection;

    public DeleteData(DatabaseConnection dConnection) {
        this.connection = dConnection.getConnection();
    }

    public void deleteRecording() throws SQLException {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter compilation ID");
        int compilationID = scanner.nextInt();

        System.out.println("Enter performance ID");
        int performanceID = scanner.nextInt();

        CallableStatement cs = connection.prepareCall("call delete_music_data.delete_recording(?,?)");
        cs.setInt(1,performanceID);
        cs.setInt(2,compilationID);
        cs.execute();
        cs.close();
    }

    public void deletePerformance() throws SQLException {
        CallableStatement cs = connection.prepareCall("call delete_music_data.delete_performance(?)");
        cs.setInt(1, Utils.getID());
        cs.execute();
        cs.close();
    }

    public void deleteContributor() throws SQLException {
        CallableStatement cs = connection.prepareCall("call delete_music_data.delete_contributor(?)");
        cs.setInt(1, Utils.getID());
        cs.execute();
        cs.close();
    }

    public void deleteContribution() throws SQLException {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter role ID");
        int roleID = scanner.nextInt();

        System.out.println("Enter contributor ID");
        int contributorID = scanner.nextInt();

        System.out.println("Enter performance ID");
        int performanceID = scanner.nextInt();

        CallableStatement cs = connection.prepareCall("call delete_music_data.delete_contribution(?, ?, ?)");
        cs.setInt(1, roleID);
        cs.setInt(2, contributorID);
        cs.setInt(3, performanceID);
        cs.execute();
        cs.close();
    }
}

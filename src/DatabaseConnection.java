import java.sql.DriverManager;
import java.sql.SQLException;

import java.sql.Connection;
/**
 * Establish the connection to the database and return it
 **/
public class DatabaseConnection {
    private Connection connection;

    public DatabaseConnection(String url, String username, String password) throws SQLException {
        this.connection = DriverManager.getConnection(url, username, password);
    }

    public Connection getConnection() {
        return connection;
    }
}

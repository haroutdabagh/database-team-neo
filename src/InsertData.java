import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.SQLException;
import java.util.Scanner;

public class InsertData {
    private Connection connection;
    private Scanner scanner;

    public InsertData(DatabaseConnection dbConnection) {
        this.connection = dbConnection.getConnection();
        scanner = new Scanner(System.in);
    }

    /***
     * Inserts a recording in the database
     * @param durationUsed
     * @param offset
     * @param performanceId
     * @throws SQLException
     */
    public void insertRecording() throws SQLException {
        System.out.println("Enter compilation id");
        int compilationId = scanner.nextInt();

        System.out.println("Enter duration used");
        int durationUsed = scanner.nextInt();

        System.out.println("Enter offset");
        int offset = scanner.nextInt();

        System.out.println("Enter performance id");
        int performanceId = scanner.nextInt();

        CallableStatement cs = connection.prepareCall("call update_music_data.insert_new_recording(?, ?, ?, ?)");
        cs.setInt(1, compilationId);
        cs.setInt(2, durationUsed);
        cs.setInt(3, offset);
        cs.setInt(4, performanceId);
        cs.execute();
        cs.close();
    }

   /***
     * Inserts a new performance into the database.
     * @param name
     * @param offset
     * @param duration
     * @throws SQLException
     */
    public void insertPerformance() throws SQLException {
        System.out.println("Enter recording date (YYYY-MM-DD)");
        Date recordingDate = Date.valueOf(scanner.next());
        System.out.println("Enter offset");
        int offset = scanner.nextInt();

        System.out.println("Enter duration");
        int duration = scanner.nextInt();

        CallableStatement cs = connection.prepareCall("call update_music_data.insert_new_performance(?, ?, ?)");
        cs.setDate(1, recordingDate);
        cs.setInt(2, offset);
        cs.setInt(3, duration);
        cs.execute();
        cs.close();
    }

    /***
     * Inserts a new contributor
     * @param stageName
     * @param lastName
     * @param firstName
     * @throws SQLException
     */
    public void insertNewContributor() throws SQLException {
        System.out.println("Enter contributor name");
        String name = scanner.nextLine();

        CallableStatement cs = connection.prepareCall("call update_music_data.insert_new_contributor(?)");
        cs.setString(1, name);
        cs.execute();
        cs.close();
    }

     /***
     * Insert a new contribution
     * @param roleID
     * @param contributorId
     * @param performanceID
     * @throws SQLException
     */
    public void insertNewContribution() throws SQLException {
        System.out.println("Enter roleID");
        int roleID = scanner.nextInt();

        System.out.println("Enter contributorID");
        int contributorID = scanner.nextInt();

        System.out.println("Enter performanceID");
        int performanceID = scanner.nextInt();

        CallableStatement cs = connection.prepareCall("call update_music_data.insert_new_contribution(?, ?, ?)");
        cs.setInt(1, roleID);
        cs.setInt(2, contributorID);
        cs.setInt(3, performanceID);
        cs.execute();
        cs.close();
    }

}

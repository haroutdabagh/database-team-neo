CREATE OR REPLACE PROCEDURE recording_data_check  (
    p_compilation_id IN performance.performance_id%TYPE,
    p_performance_id IN performance.performance_id%TYPE,
    p_duration_used IN recording.DURATION_USED%TYPE)
AS
    recording_self_link EXCEPTION;
    PRAGMA EXCEPTION_INIT(recording_self_link,-20025);

    over_max_duration   EXCEPTION;
    pragma              EXCEPTION_INIT(over_max_duration,-20026);
    performance_duration performance.duration%TYPE;
BEGIN
    -- check self joins
    IF p_compilation_id = p_performance_id THEN    
        raise_application_error(-20025,'Cannot insert recording with same id');
    END IF;
    
    -- check if the duration used does not exceed the songs duration
    SELECT duration INTO performance_duration 
    FROM performance
    WHERE performance_id = p_performance_id;
    DBMS_OUTPUT.put_line(performance_duration);

    IF p_duration_used > performance_duration THEN
        raise_application_error(-20026,'Duration used exceeds performance duration');
    END IF;
END; 
/
CREATE OR REPLACE PROCEDURE insert_log(
    p_action IN user_log.action%TYPE,
    p_affected IN user_log.affected%TYPE,
    is_success  boolean
)
AS
    curr_user       user_log.user_id%TYPE;
    p_result        user_log.result%TYPE;
BEGIN
    -- get current user 
    SELECT sys_context('USERENV', 'CURRENT_USER') INTO curr_user FROM dual;
    -- set result string
    IF(is_success) THEN
        p_result := 'Success';
    ELSE
        p_result := 'Failure';
    END IF;
    -- log
    INSERT INTO user_log(
        user_id,
        action,
        affected,
        result,
        time_at
    )
    VALUES(
        curr_user,
        p_action,
        p_affected,
        p_result,
        CURRENT_TIMESTAMP
    );
    COMMIT;
END;
/
SELECT * FROM contributor;

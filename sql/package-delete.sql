CREATE OR REPLACE PACKAGE delete_music_data AS
    PROCEDURE delete_recording (
        p_performance_id IN recording.performance_id%TYPE,
        p_compilation_id IN recording.performance_id%TYPE
    );

    PROCEDURE delete_performance (
        p_performance_id IN performance.performance_id%TYPE
    );

    PROCEDURE delete_contributor (
        p_contributor_id IN contributor.contributor_id%TYPE
    );

    PROCEDURE delete_contribution (
        p_role_id         IN  contribution.role_id%TYPE,
        p_contributor_id  IN  contribution.contributor_id%TYPE,
        p_performance_id  IN  contribution.performance_id%TYPE
    );

END delete_music_data;
/

CREATE OR REPLACE PACKAGE BODY delete_music_data AS
    PROCEDURE delete_recording (
        p_performance_id IN recording.performance_id%TYPE,
        p_compilation_id IN recording.performance_id%TYPE
    )
    AS
    BEGIN        
        -- check if they exist first
        check_music_data.check_recording_exists(p_compilation_id,p_performance_id);
        -- try to delete
        DELETE FROM RECORDING
        WHERE
            COMPILATION_ID = p_compilation_id AND performance_id = p_performance_id;  
        -- log to user log        
        insert_log('Delete','Recording',true);
    EXCEPTION -- if exception then log failure        
        WHEN OTHERS THEN
            ROLLBACK; -- force rollback
            insert_log('Delete','Recording',false);
            RAISE;
    END;

    PROCEDURE delete_performance (
        p_performance_id IN performance.performance_id%TYPE
    )
    AS
    BEGIN
        -- check if performance exists first
        check_music_data.check_performance_exists(p_performance_id);
        -- try delete
        DELETE FROM PERFORMANCE
        WHERE
            PERFORMANCE_ID = p_performance_id; 
        -- log action  
        insert_log('Delete','Performance',true);
    EXCEPTION
        WHEN OTHERS THEN
            ROLLBACK;
            insert_log('Delete','Performance',false);
            RAISE;
    END;

    PROCEDURE delete_contributor (
        p_contributor_id IN contributor.contributor_id%TYPE
    )
    AS
    BEGIN
        -- check if contributor exists first
        check_music_data.check_contributor_exists(p_contributor_id);
        
        DELETE FROM CONTRIBUTOR
        WHERE
            CONTRIBUTOR_ID = p_contributor_id; 
        -- log action    
        insert_log('Delete','Contributor',true);
    EXCEPTION
        WHEN OTHERS THEN
            ROLLBACK;
            insert_log('Delete','Contributor',false);
            RAISE;
    END;

    PROCEDURE delete_contribution (
        p_role_id         IN  contribution.role_id%TYPE,
        p_contributor_id  IN  contribution.contributor_id%TYPE,
        p_performance_id  IN  contribution.performance_id%TYPE
    )
    AS
    BEGIN
        check_music_data.check_contribution_exists(p_contributor_id,p_performance_id,p_role_id);
        DELETE FROM CONTRIBUTION
        WHERE
            PERFORMANCE_ID = p_performance_id AND contributor_id = p_contributor_id AND role_id=p_role_id; 
        -- log action 
        insert_log('Delete','Contribution',true);
    EXCEPTION
        WHEN OTHERS THEN
            ROLLBACK;
            insert_log('Delete','Contribution',false);    
            RAISE;
    END;

END delete_music_data;
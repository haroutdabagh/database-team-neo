CREATE OR REPLACE PACKAGE update_music_data AS
    recording_pk_exists     EXCEPTION;
    contribution_pk_exists  EXCEPTION;

    PRAGMA              EXCEPTION_INIT(recording_pk_exists,-20034);  
    PRAGMA              EXCEPTION_INIT(contribution_pk_exists,-20035);

    PROCEDURE insert_new_recording (
        p_compilation_id IN recording.compilation_id%TYPE,
        p_duration_used   IN  recording.duration_used%TYPE,
        p_offset         IN  recording.offset%TYPE,
        p_performance_id  IN  recording.performance_id%TYPE
    );

    PROCEDURE insert_new_performance (
        p_recording_date  IN  performance.recording_date%TYPE,
        p_offset         IN  performance.offset%TYPE,
        p_duration       IN  performance.duration%TYPE
    );

    PROCEDURE insert_new_contributor (
        p_name  IN  contributor.name%TYPE
    );

    -- inserts a new contribution with a role
    PROCEDURE insert_new_contribution (
        p_role_id         IN  contribution.role_id%TYPE,
        p_contributor_id  IN  contribution.contributor_id%TYPE,
        p_performance_id  IN  contribution.performance_id%TYPE
    );
END update_music_data;

/
CREATE OR REPLACE PACKAGE BODY update_music_data AS
    PROCEDURE insert_new_recording (
        p_compilation_id IN recording.compilation_id%TYPE,
        p_duration_used   IN  recording.duration_used%TYPE,
        p_offset         IN  recording.offset%TYPE,
        p_performance_id  IN  recording.performance_id%TYPE
    )
    AS
    BEGIN  
        -- check if it exists       
        IF(check_music_data.recording_exists(p_compilation_id,p_performance_id)) THEN
            raise_application_error(-20034,'Recording '|| p_compilation_id ||', '|| p_performance_id || ' already exists');
        END IF;
        -- insert if all good
        INSERT INTO recording(
            COMPILATION_ID,
            DURATION_USED,
            OFFSET,
            PERFORMANCE_ID
        )
        VALUES
        (
            p_compilation_id,
            p_duration_used,
            p_offset,
            p_performance_id
        );
        -- log action
        insert_log('Update','Recording',true);
    EXCEPTION 
        WHEN OTHERS THEN -- log failure
            ROLLBACK; -- rollback incase
            insert_log('Update','Recording',false); 
            RAISE;
    END;

    PROCEDURE insert_new_performance (
        p_recording_date  IN  performance.recording_date%TYPE,
        p_offset         IN  performance.offset%TYPE,
        p_duration       IN  performance.duration%TYPE
    )
    AS
    BEGIN         
        INSERT INTO PERFORMANCE(
            RECORDING_DATE,
            OFFSET,
            "DURATION"
        )
        VALUES
        (
            p_recording_date,
            p_offset,
            p_duration
        );
        -- log action
        insert_log('Update','Performance',true);
    EXCEPTION
        WHEN OTHERS THEN
            ROLLBACK;
            insert_log('Update','Performance',false);  
            RAISE;
    END;

    PROCEDURE insert_new_contributor (
        p_name  IN  contributor.name%TYPE
    )
    AS
    BEGIN        
        INSERT INTO CONTRIBUTOR("NAME")
        VALUES (p_name);
        insert_log('Update','Contributor',true);
    EXCEPTION
        WHEN OTHERS THEN
            ROLLBACK;
            insert_log('Update','Contributor',false);  
            RAISE; 
    END;

    PROCEDURE insert_new_contribution (
        p_role_id         IN  contribution.role_id%TYPE,
        p_contributor_id  IN  contribution.contributor_id%TYPE,
        p_performance_id  IN  contribution.performance_id%TYPE
    )
    AS
    BEGIN  
        -- check if it exists       
        IF(check_music_data.contribution_exists(p_contributor_id,p_performance_id,p_role_id)) THEN
            raise_application_error(-20035,'Contribution '|| p_contributor_id ||', '|| p_performance_id || p_contributor_id ||', '|| p_role_id ||' already exists');
        END IF;
        -- insert if all good
        INSERT INTO CONTRIBUTION(
            ROLE_ID,
            CONTRIBUTOR_ID,
            PERFORMANCE_ID
        )
        VALUES
        (
            p_role_id,
            p_contributor_id,
            p_performance_id
        ); 
        -- log action
        insert_log('Update','Contribution',true);
    EXCEPTION
        WHEN OTHERS THEN
            ROLLBACK;
            insert_log('Update','Contribution',false);      
            RAISE;  
    END;

END update_music_data;

/

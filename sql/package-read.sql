CREATE OR REPLACE PACKAGE fetch_music_data AS
    -- cursors for returning information
    TYPE performance_cursor IS REF CURSOR RETURN performance%ROWTYPE;
    TYPE contributors_cursor IS REF CURSOR RETURN contributor%ROWTYPE;
    TYPE roles_cursor IS REF CURSOR RETURN role%ROWTYPE;   
    
    -- retrieves all compilations and recordings inside a compilation
    PROCEDURE get_compilation_performances(
        p_compilation_id IN performance.performance_id%TYPE,
        p_performances OUT performance_cursor);

    -- retrieves contributions in a performance
    PROCEDURE get_performance_contributons(
        p_performance_id IN performance.performance_id%TYPE, 
        p_contributors_roles OUT SYS_REFCURSOR);

    -- retrieves contributions made by a contributor                                     
    PROCEDURE get_contributor_contributions(
        p_contributor_id IN contributor.contributor_id%TYPE, 
        p_performances_roles OUT SYS_REFCURSOR);

    -- retrieves contributor roles
    PROCEDURE get_all_contributor_roles(
        p_contributor_id IN contributor.contributor_id%TYPE,
        p_roles OUT roles_cursor);

    -- retrieves all compilations (performances with recordings inside them)
    PROCEDURE get_all_compilations(
        p_performances OUT performance_cursor);

END fetch_music_data;

/
CREATE OR REPLACE PACKAGE BODY fetch_music_data AS

  -- Add procedure body
    PROCEDURE get_compilation_performances(
        p_compilation_id IN performance.performance_id%TYPE,
        p_performances OUT performance_cursor)
    AS     
        not_a_compilation   EXCEPTION;
        PRAGMA              EXCEPTION_INIT(not_a_compilation,-20001);   

        performance_count   int; 
    BEGIN        
        -- count entries and check
        SELECT COUNT(*) INTO performance_count
        FROM recording
        WHERE compilation_id = p_compilation_id;
        IF performance_count = 0 THEN
            raise_application_error(-20001,'Compilation '|| p_compilation_id || ' is not a valid compilation');
        END IF;  
        -- open the cursor
        OPEN p_performances FOR 
            SELECT p.performance_id, p.duration, p.offset, p.recording_date FROM recording r 
            INNER JOIN performance p ON r.performance_id = p.performance_id
            WHERE compilation_id = p_compilation_id;  
        insert_log('Read','Performance',true);
    EXCEPTION
        WHEN OTHERS THEN
            insert_log('Read','Performance',false);
            RAISE;
    END;

    PROCEDURE get_performance_contributons(
        p_performance_id IN performance.performance_id%TYPE, 
        p_contributors_roles OUT SYS_REFCURSOR)
    AS
    BEGIN
        -- check if the performance exists
        check_music_data.check_performance_exists(p_performance_id);
        -- open the cursor
        OPEN p_contributors_roles FOR 
            SELECT co.contributor_id, co.name AS contributor_name,  -- contributor row
                r.role_id, r.name AS role_name -- role row
            FROM contribution cs
            -- inner join to get data
            INNER JOIN role r ON cs.role_id = r.role_id
            INNER JOIN contributor co ON cs.contributor_id = co.contributor_id
            WHERE performance_id=p_performance_id;  
        insert_log('Read','Contribution',true);
    EXCEPTION
        WHEN OTHERS THEN
            insert_log('Read','Contribution',false);
            RAISE;
    END;                                                

    PROCEDURE get_contributor_contributions(p_contributor_id IN contributor.contributor_id%TYPE, 
                                            p_performances_roles OUT SYS_REFCURSOR)
    AS
    BEGIN
        -- check contributor
        check_music_data.check_contributor_exists(p_contributor_id);
        -- open cursor
        OPEN p_performances_roles FOR 
            SELECT p.performance_id, p.duration,p.offset, p.recording_date, -- performance row
            r.role_id, r.name -- role row
            FROM contribution cs
            -- joins to get roles and performances
            INNER JOIN role r ON cs.role_id = r.role_id
            INNER JOIN performance p ON p.performance_id = cs.performance_id
            WHERE contributor_id=p_contributor_id;  
        insert_log('Read','Contribution',true);
    EXCEPTION
        WHEN OTHERS THEN
            insert_log('Read','Contribution',false);
            RAISE;
    END;

    PROCEDURE get_all_contributor_roles(
        p_contributor_id IN contributor.contributor_id%TYPE,
        p_roles OUT roles_cursor)
    AS
    BEGIN
        -- check contributor
        check_music_data.check_contributor_exists(p_contributor_id);
        -- open cursor
        OPEN p_roles FOR 
            -- you dont want the roles to repeat
            SELECT UNIQUE r.role_id, r.name
            FROM contribution cs
            -- get the roles from the contributions            
            INNER JOIN role r ON cs.role_id = r.role_id
            WHERE contributor_id=p_contributor_id;  
        insert_log('Read','Contributor',true);
    EXCEPTION
        WHEN OTHERS THEN
            insert_log('Read','Contributor',false);
            RAISE;
    END;
    
    PROCEDURE get_all_compilations(
        p_performances OUT performance_cursor)
    AS
    BEGIN
        OPEN p_performances FOR 
            -- get the performances
            SELECT DISTINCT p.performance_id, p.duration, p.offset, p.recording_date
            FROM performance p
            INNER JOIN recording r ON r.compilation_id = p.performance_id;  
        insert_log('Read','Performance',true);
    EXCEPTION
        WHEN OTHERS THEN
            insert_log('Read','Performance',false);
            RAISE;
    END;              
END fetch_music_data;
/


CREATE TABLE user_log
(
  user_id varchar2(32) NOT NULL,
  action varchar2(16) NOT NULL,
  affected varchar2(16) NOT NULL,
  result varchar2(8) NOT NULL,
  time_at timestamp NOT NULL 
);

CREATE TABLE contribution
(
  contributor_id number(4,0) NOT NULL,
  role_id number(4,0) NOT NULL,
  performance_id number(4,0) NOT NULL
);

CREATE SEQUENCE contributor_id_seq
MINVALUE 1
MAXVALUE 9999;

CREATE TABLE contributor
(
  contributor_id number(4,0) DEFAULT contributor_id_seq.NEXTVAL PRIMARY KEY,
  name varchar2(32) NOT NULL
);

CREATE SEQUENCE market_id_seq
MINVALUE 1
MAXVALUE 9999;

CREATE TABLE market
(
  market_id number(4,0) DEFAULT market_id_seq.NEXTVAL PRIMARY KEY ,
  location varchar2(32) NOT NULL
);

CREATE SEQUENCE perf_id_seq
MINVALUE 1
MAXVALUE 9999;

CREATE TABLE performance
(
  performance_id number(4,0) DEFAULT perf_id_seq.NEXTVAL PRIMARY KEY,
  duration number(8,2) DEFAULT 0.01 NOT NULL CHECK (duration > 0),
  offset number(8,2) DEFAULT 0 NOT NULL CHECK (offset >= 0),
  recording_date timestamp NOT NULL
);

CREATE SEQUENCE label_id_seq
MINVALUE 1
MAXVALUE 9999;

CREATE TABLE record_label
(
  record_label_id number(4,0) DEFAULT label_id_seq.NEXTVAL PRIMARY KEY ,
  name varchar2(32) NOT NULL
);

CREATE TABLE recording
(
  compilation_id number(4,0) NOT NULL,
  performance_id number(4,0) NOT NULL,
  offset number(8,2) DEFAULT 0 NOT NULL CHECK (offset >= 0),
  duration_used number(8,2) DEFAULT 0 NOT NULL CHECK (duration_used > 0)
  
);

CREATE SEQUENCE release_id_seq
MINVALUE 1
MAXVALUE 9999;

CREATE TABLE release
(
  release_id number(4,0) DEFAULT release_id_seq.NEXTVAL PRIMARY KEY,
  name varchar2(32) NOT NULL,
  compilation_id number(4,0) NOT NULL,
  release_date timestamp NOT NULL,
  record_label_id number(4,0) NOT NULL
);

CREATE TABLE release_market
(
  release_id number(4,0) NOT NULL, 
  market_id number(4,0) NOT NULL,
  CONSTRAINT fk_release_market_release 
    FOREIGN KEY (release_id) REFERENCES release(release_id),
  CONSTRAINT fk_release_market_market   
    FOREIGN KEY (market_id) REFERENCES market(market_id)
);

CREATE SEQUENCE role_id_seq
MINVALUE 1
MAXVALUE 9999;

CREATE TABLE role
(
  role_id number(4,0) DEFAULT role_id_seq.NEXTVAL PRIMARY KEY,
  name varchar2(32) NOT NULL 
);

ALTER TABLE contribution ADD CONSTRAINT fk_contribution_contributor
  FOREIGN KEY (contributor_id) REFERENCES contributor (contributor_id) ON DELETE CASCADE;

ALTER TABLE contribution ADD CONSTRAINT fk_contribution_performance
  FOREIGN KEY (performance_id) REFERENCES performance (performance_id) ON DELETE CASCADE;

ALTER TABLE contribution ADD CONSTRAINT fk_contribution_role
  FOREIGN KEY (role_id) REFERENCES role (role_id) ON DELETE CASCADE;

ALTER TABLE recording ADD CONSTRAINT fk_recording_compilation
  FOREIGN KEY (compilation_id) REFERENCES performance (performance_id) ON DELETE CASCADE;

ALTER TABLE recording ADD CONSTRAINT fk_recording_recording
  FOREIGN KEY (performance_id) REFERENCES performance (performance_id);

ALTER TABLE release ADD CONSTRAINT fk_release_compilation
  FOREIGN KEY (release_id) REFERENCES performance (performance_id) ON DELETE CASCADE;

ALTER TABLE contribution ADD CONSTRAINT unique_contribution_triple_key
  UNIQUE(performance_id,role_id,contributor_id);

ALTER TABLE recording ADD CONSTRAINT unique_recording_compilation
  UNIQUE(compilation_id,performance_id);
 
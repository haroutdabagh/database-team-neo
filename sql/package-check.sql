CREATE OR REPLACE PACKAGE check_music_data AS
    -- exception handling for performances
    no_such_performance EXCEPTION;
    no_such_contributor EXCEPTION;
    no_such_role EXCEPTION;
    no_such_contribution EXCEPTION;
    no_such_recording EXCEPTION;
    PRAGMA              EXCEPTION_INIT(no_such_performance,-20004);  
    PRAGMA              EXCEPTION_INIT(no_such_contributor,-20005);  
    PRAGMA              EXCEPTION_INIT(no_such_role,-20006);  
    PRAGMA              EXCEPTION_INIT(no_such_contribution,-20007);  
    PRAGMA              EXCEPTION_INIT(no_such_recording,-20008);  
    
    -- returns true if the contributor exists
    FUNCTION contributor_exists(
        p_contributor_id IN contributor.contributor_id%TYPE 
    )
    RETURN boolean;
    -- returns true if the performance exists
    FUNCTION performance_exists(
        p_performance_id IN performance.performance_id%TYPE 
    )
    RETURN boolean;
    -- returns true if the role exists    
    FUNCTION role_exists(
        p_role_id IN role.role_id%TYPE
    )
    RETURN boolean;

    -- returns true if there is an entry in the recording joint table
    FUNCTION recording_exists(
        p_compilation_id IN performance.performance_id%TYPE, 
        p_performance_id IN performance.performance_id%TYPE 
    )
    RETURN boolean;

    -- returns true if theres an entry in the contribution joint table
    FUNCTION contribution_exists(
        p_contributor_id IN contributor.contributor_id%TYPE, 
        p_performance_id IN performance.performance_id%TYPE, 
        p_role_id IN role.role_id%TYPE
    )
    RETURN boolean;

    -- exception for checking if a contributor existse
    PROCEDURE check_contributor_exists (
        p_contributor_id IN contributor.contributor_id%TYPE 
    );
    -- exception for checking if a performance exists
    PROCEDURE check_performance_exists (
        p_performance_id IN performance.performance_id%TYPE 
    );   
    -- exception for checking if a role exists
    PROCEDURE check_role_exists (
        p_role_id IN role.role_id%TYPE
    ); 
    -- exception for checking if a contribution exists
    PROCEDURE check_contribution_exists (
        p_contributor_id IN contributor.contributor_id%TYPE ,
        p_performance_id IN performance.performance_id%TYPE ,
        p_role_id IN role.role_id%TYPE
    ); 
    -- exception for checking if a recording exists
    PROCEDURE check_recording_exists (
        p_compilation_id IN performance.performance_id%TYPE ,
        p_performance_id IN performance.performance_id%TYPE 
    ); 
END check_music_data;
/
CREATE OR REPLACE PACKAGE BODY check_music_data AS  
    -- returns true if the contributor exists
    FUNCTION contributor_exists(
        p_contributor_id IN contributor.contributor_id%TYPE 
    )
    RETURN boolean
    AS
        contributor_count int;
    BEGIN
        -- check if it exists with an aggregation
        SELECT COUNT(*) INTO contributor_count 
        FROM contributor
        WHERE contributor_id = p_contributor_id;

        return (contributor_count > 0);
    END;

    -- returns true if the performance exists
    FUNCTION performance_exists(
        p_performance_id IN performance.performance_id%TYPE 
    )    RETURN boolean
    AS
        performance_count int;
    BEGIN
        SELECT COUNT(*) INTO performance_count 
        FROM performance
        WHERE performance_id = p_performance_id;

        return (performance_count > 0);
    END;

    -- returns true if the role exists    
    FUNCTION role_exists(
        p_role_id IN role.role_id%TYPE
    )    RETURN boolean
    AS
        role_count int;
    BEGIN
        SELECT COUNT(*) INTO role_count 
        FROM role
        WHERE role_id = p_role_id;
        return (role_count > 0);
    END;

    -- returns true if there is an entry in the recording joint table
    FUNCTION recording_exists(
        p_compilation_id IN performance.performance_id%TYPE ,
        p_performance_id IN performance.performance_id%TYPE 
    )
    RETURN boolean    
    AS
        recording_count int;
    BEGIN
        SELECT COUNT(*) INTO recording_count 
        FROM recording
        WHERE performance_id = p_performance_id AND COMPILATION_ID = p_compilation_id;

        return (recording_count > 0);
    END;

    -- returns true if theres an entry in the contribution joint table
    FUNCTION contribution_exists(
        p_contributor_id IN contributor.contributor_id%TYPE ,
        p_performance_id IN performance.performance_id%TYPE ,
        p_role_id IN role.role_id%TYPE
    )
    RETURN boolean
    AS
        contribution_count int;
    BEGIN
        SELECT COUNT(*) INTO contribution_count 
        FROM contribution
        WHERE CONTRIBUTOR_ID = p_contributor_id AND ROLE_ID = p_role_id AND PERFORMANCE_ID = p_performance_id;

        return (contribution_count > 0);
    END;

    -- exception handling for checking if a contributor existse
    PROCEDURE check_contributor_exists (
        p_contributor_id IN contributor.contributor_id%TYPE 
    )
    AS
    BEGIN
        IF(NOT contributor_exists(p_contributor_id)) THEN
            raise_application_error(-20005,'Contributor '|| p_contributor_id || ' is not a valid contributor');
        END IF;
    END;
    -- exception handling for checking if a performance exists
    PROCEDURE check_performance_exists (
        p_performance_id IN performance.performance_id%TYPE 
    )
    AS
    BEGIN
        IF(NOT performance_exists(p_performance_id)) THEN
            raise_application_error(-20005,'Performance '|| p_performance_id || ' is not a valid performance');
        END IF;
    END;  
    -- exception handling for checking if a role exists
    PROCEDURE check_role_exists (
        p_role_id IN role.role_id%TYPE
    )
    AS
    BEGIN
        IF(NOT role_exists(p_role_id)) THEN
            raise_application_error(-20006,'Role '|| p_role_id || ' is not a valid role');
        END IF;
    END;
        -- exception for checking if a contribution exists
    PROCEDURE check_contribution_exists (
        p_contributor_id IN contributor.contributor_id%TYPE ,
        p_performance_id IN performance.performance_id%TYPE ,
        p_role_id IN role.role_id%TYPE
    )
    AS
    BEGIN
        IF(NOT contribution_exists(p_contributor_id,p_performance_id,p_role_id)) THEN
            raise_application_error(-20007,'Contribution '|| p_contributor_id ||', '|| p_performance_id || p_contributor_id ||', '|| p_role_id ||' is not a valid contribution');
        END IF;
    END;

    -- exception for checking if a recording exists
    PROCEDURE check_recording_exists (
        p_compilation_id IN performance.performance_id%TYPE ,
        p_performance_id IN performance.performance_id%TYPE 
    )
    AS
    BEGIN
        IF(NOT recording_exists(p_compilation_id,p_performance_id)) THEN
            raise_application_error(-20008,'Recording '|| p_compilation_id ||', '|| p_performance_id || ' is not a valid recording');
        END IF;
    END; 
END check_music_data;
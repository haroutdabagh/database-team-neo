-- Compilation before insert, check if there's any joins to itself(which should be illegal)

CREATE OR REPLACE TRIGGER before_insert_recording
  BEFORE INSERT ON recording
  FOR EACH ROW
  BEGIN
    recording_data_check(:NEW.compilation_id,:NEW.performance_id,:NEW.duration_used);
  END;
/

CREATE OR REPLACE TRIGGER before_update_recording
  BEFORE UPDATE ON recording
  FOR EACH ROW
  BEGIN
    recording_data_check(:NEW.compilation_id,:NEW.performance_id,:NEW.duration_used);
  END;
/
-- Compilation trigger
CREATE OR REPLACE TRIGGER after_insert_recording_update
  AFTER INSERT ON recording
  FOR EACH ROW
  DECLARE    
    added_duration    performance.duration%TYPE;
  BEGIN
    added_duration := NVL(:NEW.duration_used,0) + NVL(:NEW.offset,0);

    -- update the performance table
    UPDATE performance
    SET duration = duration + added_duration 
    WHERE performance_id = :NEW.compilation_id;
  END after_insert_recording_update;
/